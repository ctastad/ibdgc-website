---
# Adapted from themes/academic/archetypes/project/index.md
title: "Data Coordinating Center"
subtitle: "Icahn School of Medicine at Mount Sinai and the University of Chicago"
pi: "Judy Cho, MD"
pi_titles: ["Principal Investigator, IBDGC Data Coordinating Center", "Professor, Icahn School of Medicaine at Mount Sinai"]
picture: "/img/mssm/judy-cho-pic.jpg"
bio: ["Dr. Cho has served as the Principal Investigator of the NIDDK IBDGC since 2003. She plays a lead role in setting scientific and operational priorities, assessing new sample and analytic approaches, and overseeing internal and external communication."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "https://icahn.mssm.edu/profiles/judy-h-cho"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 12
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# DCC Objectives

The Data Coordinating Center (DCC) of the NIDDK IBDGC plays a lead role in a) assessing and applying innovative research approaches in genetics and genomics to define pathophysiologic mechanisms, b) operationally unifying the NIDDK IBDGC and assuring rigor and reproducibility at scale, and c) facilitating collaboration with external investigators and groups to maximize the size and scientific value of existing datasets, provide complementary expertise, and develop the next generation of IBD investigators pursuing genetic, cellular and molecular understanding of IBD.

---

# Specific Aims: 2017-2022

**Aim 1:** To apply new laboratory and computing technologies and collect accurate, detailed data on disease pathogenesis from the clinical all the way down to the cellular level and genetic level, sharing with the broader research community.
- Expand platforms for collecting detailed, longitudinal cohort collections
- Efficiently collaborate with top laboratories, operationalizing sample and data flows and linking results to genetics
- Develop and scale objective assessments of disease activity and treatment including automated methods for extracting information from endoscopy and pathology reports, virtual slides of endoscopic biopsies, endoscopic video and Electronic Medical Records (EMRs)
- Develop and apply a cloud-based IBD Data Commons, for broad investigator use

**Aim 2:** To prioritize scientific directions for the Consortium and scale new technologies, sampling strategies and analyses to larger cohorts, including longitudinal data.
- To fine-map established loci by expanding sample size and diversity. To distinguish credible SNPs between CD and UC
- Comprehensively define the genetic basis for altered intestinal epithelial gene expression and function in UC. We will analyze sources of variation in mRNA expression from spheroids grown from biopsies taken from inflamed and uninflamed UC colon.
- Determine the genetic basis for how pathophysiologic mechanisms function over time, including in response to therapeutic interventions

---

# Ongoing and New Directions

### New recruitment priorities and approaches

The DCC is piloting methods to facilitate recruitment in non-European populations using single IRBs, alternative reimbursement strategies, biobank-based and direct-to-patient recruitment approaches. The DCC leads a monthly coordinators’ meeting to communicate IBDGC priorities, assess progress and share effective recruiting and sample handling approaches across the Consortium.

### New data types and accelerating integration

The DCC is adding to its form-based methods for collecting phenotype data by using electronic medical records and diagnostic imaging and reports. IBDGC investigators are piloting image-based measurements beginning with radiology but likely expanding into both pathology and paraffin-based methods as well. Rapidly developing applications of multi-omic single cell technologies are a high priority, especially for their ability to provide insight into IBD physiology.

### Data sharing, data mining, and project acceleration

Common variant, GWAS-based data mapped to traits represents an important, foundation for further studies. Expansions of both chip and sequence-based cohorts will be finalized in 2021-2022 and published via dbGaP and the IBDGC Data Commons. The integration of African-based reference, common variant data substantially improves IBD trait prediction as an important basis for the expansion of non-European cohorts. Tissue-based, bulk RNAseq gene expression linked to important clinical inflection points has provided substantial mechanistic insight, especially when integrated with single cell data. Through the DCC, the IBDGC will utilize the Data Commons to increase the speed of analyses and data sharing in promotion of deeper collaboration.

### Training the next generation of IBD researchers

The DCC organizes a rotating schedule of GRC presentations where work from junior investigators is encouraged. In 2020, the DCC initiated a webinar series focusing on data-intensive, translational investigators. Given a) the rapid rate of data expansion in scope and type, and b) the increasingly refined mining capacities of extant clinical collections, the opportunities for training and developing the next generation of translational researchers are highly promising.
