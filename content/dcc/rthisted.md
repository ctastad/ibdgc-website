---
name: "Ronald Thisted"
title: "Professor, University of Chicago"
role: "staff"
picture: "/img/dcc/ron-thisted.png"
weight: 20
---

Dr. Thisted has extensive expertise in biostatistics and epidemiology especially as applied to assessment of therapeutic interventions. He is also an expert in statistical computation. He has advised the DCC on its structure and operations including management of collaborative projects since 2003.
