---
name: "Ke Hao, PhD"
role: "Associate Professor, Genetics and Genomics Sciences"
picture: "/img/mssm/ke.hao.png"
weight: 30
---

Dr. Hao is currently an associate professor of the Department of Genetics and Genomic Sciences and a member of Icahn Institute of Data Science and Genomic Technology. Dr. Hao received his ScD degree and postdoc training at Harvard University and has extensive expertise in statistical genetics, computational biology and environmental health. Over the past decade, Dr. Hao has contributed significantly in these areas.   He systematically collected large datasets of human tissue samples, and generated molecular trait quantitative loci (xQTLs), including adipose, blood vessel wall, skeleton muscle, lung, liver, brain, placenta, intestine, whole blood, monocyte, macrophage, etc.  Further he integrated the xQTLs with large GWAS data to identify genetic basis of human diseases and discover the mechanism: genetic variants → molecular/cellular alternation → disease.
