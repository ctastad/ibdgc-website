---
# Adapted from themes/academic/archetypes/project/index.md
title: "Icahn School of Medicine at Mount Sinai"
subtitle: "IBD Genetics Research Center"
pi: "Judy Cho, MD"
pi_titles: ["Principal Investigator, IBDGC Data Coordinating Center"]
picture: "/img/mssm/judy-cho-pic.jpg"
bio: ["My major research efforts currently are: a) defining mechanisms of stromal/epithelial-immune cell cross talk in IBD; b) defining components of treatment refractory (specifically, non-response to pro-inflammatory cytokine blockade) IBD at the cellular and tissue level; c) predicting differences in clinical outcomes of interest across IBD clinical subtypes and populations; d) exploring the role of rare, IBD-associated variants of high effects; e) organizing large datasets for NIDDK IBDGC and IIBDGC; and f) developing select treatment targets based on IBD genetic and single cell expression findings. Since 2015, I have served as Director of the Personalized Medicine Institute at Mount Sinai. I am extremely excited by the explosion of data and new data tools that are accelerating collaborative efforts to benefit patients through genetic and genomic studies; I am eager to continue applying new data-approaches to benefit the NIDDK IBDGC."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "https://icahn.mssm.edu/profiles/judy-h-cho"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 12
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# Co-investigators

- Miriam Merad, MD/PhD (Technology & immunology)
- Ephraim Kenigsberg, PhD (single cell analysis)
- Ke Hao, PhD (genetic analysis)
- Bruce Sands, MD (Lead clinician)
- Yuval Itan, PhD
- Kyle Gettler, PhD

# Lead Clinicians

- Ryan Ungaro, MD, MS
- Adam Steinlauf
- Asher Kornbluth
- Peter Legnani
- James Marion
- Adam Greenstein (surgeon)
- Sergei Khaitov (surgeon)
- Jean-Fred Colombel
- Bachir Taouli (radiology)
- Pathology (Noam Harpaz)

# Lab and Project Management

- Ksenija Sabic (Lab Manager)
- Nai-Yun Hsu PhD (Senior staff scientist)

# Clinical Coordinators

- Colleen Chasteau
- Ujunwa Korie, MD, MS

---

### NIDDK IBDGC-related goals

#### Specific Aims

**Aim 1:** To function as a multi-disciplinary genetics research center (GRC) in Consortium-wide projects. To recruit populations prioritized by the NIDDK IBDGC, including non-European cohorts. To ascertain and participate in intestine-focused, cross-Consortium studies. To pilot novel clinical research approaches, to scale select approaches across the NIDDK IBDGC. To deepen longitudinal phenotype data for recruited patients.

**Aim 2:** To define intestinal single-cell expression features with anti-TNF non-response and response in ileal resection and inception Crohn’s cohorts. To identify key measurements for large cohort, longitudinal analyses.

#### Administrative Supplement

**Aim 1:** Optimize cell isolation protocols and surface marker selection for CITE-Seq studies.

**Aim 2:** To apply optimized CITE-Seq protocols to Crohn’s disease resection tissues: comparing classic lineage to GWAS-prioritized surface protein expression

**Aim 3:** Improve imputation of single cell-based insights onto larger datasets: developing new expression quantitative traits and defining pathophysiologic heterogeneity

#### Ancillary liaisons

- Yuval Itan (Mount Sinai), R01DK123530 Identifying population-specific IBD-associated mutations, genes and pathways
- Ophir Klein (UCSF), R21DK127206, Using human organoids to model IBD pathogenesis

---

#### Additional resources at Mount Sinai

- BioMe cohort: Dr. Cho manages the School’s blood-based biobank containing over 60K patients, for whom 30K have attained whole exome sequence data
- MSCCR (Mount Sinai Crohn’s and Colitis Registry): under the leadership of Dr. Sands, the MSCCR cohort contains over 1500 IBD patients for whom RNASeq are attained, with whole exome sequencing and chip data pending (ongoing at the Broad Institute, for full NIH-sharing)
- Radiology de-identification: The Imaging Institute at Mount Sinai has the capacity to de-identify all personal health identifiers from DICOM-attained radiology images.

#### Major interests

- Ileal resection cohorts: primary interests are in integrating bulk RNASeq with single cell and genetic data. We have shared de-identified radiology images with the Cedars-Sinai GRC, valuable for longitucinal studies
- Ulcerative colitis and enteroid-based studies: Dr. Hsu is studying the impact of loss-of-function mutations in reactive oxygen-generating enzymes (e.g NOX1) at the single cell level
- Multi-omic single cell analyses and data integration. We have previously published on integration of single cell data with bulk RNASeq; we are actively exploring to what extent emerging multi-omic datasets provide insight into blood-to-tissue IBD physiology. Piloting new technologies with paraffin-tissues
