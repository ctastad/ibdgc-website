---
name: "Ephraim Kenigsberg, PhD"
role: "Assistant Professor, Genetics and Genomics Science"
picture: "/img/mssm/ephraim.kenigsberg.png"
weight: 40
---

Dr. Ephraim Kenigsberg is an Assistant Professor for the Department of Genetics and Genomic Sciences and the Icahn Institute for Data Science and Genomic Technology.
