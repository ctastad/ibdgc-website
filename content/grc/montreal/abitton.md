---
name: "Dr. Alain Bitton, MD"
role: "Professor of Medicine"
picture: "/img/montreal/alain_bitton.png"
weight: 10
---

Dr. Alain Bitton is Associate Professor of Medicine at McGill University. He completed a clinical research fellowship in Inflammatory Bowel Disease (IBD) at the Beth Israel Deaconess Medical Center, Harvard Medical School from 1994 to 1996.  He was the McGill Gastroenterology Residency Program Director from 1997 to 2007. Since 2009 he has been the Director of the Division of Gastroenterology at McGill University and the McGill University Health Centre (MUHC). His research in the field of IBD focuses on the identification of clinical, biologic, psychosocial and genetic predictors of relapse and disease course in IBD.