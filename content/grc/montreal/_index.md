---
# Adapted from themes/academic/archetypes/project/index.md
title: "Montréal-Boston Collaborative"
subtitle: "IBD Genetics Research Center"
pi: "John D. Rioux, PhD"
pi_titles: ["Principal Investigator", "Canada Research Chair, Professor of Medicine", "Director, Laboratory of Genetics and Genomic Medicine of Inflammation", "Director, Integrative Biology Platform (Université de Montréal and Montreal Heart Institute)"]
picture: "/img/montreal/john-rioux.jpg"
#bio: ["Since his move to Montreal in 2005 Dr. Rioux has been the Director of the Laboratory in Genetics and Genomic Medicine of Inflammation, and more recently the Director of the MHI/UdeM Integrative Biology Platform and the High-Performance Sorting Platform. He is a founding member of multiple international consortia and currently co-leads the International IBD Genetics Consortium, is Chair of the Steering Committee of the NIDDK IBD Genetics Consortium and is the leader of the IBD Genomic Medicine (iGenoMed) Consortium.", "Dr. Rioux’s research focuses on three main areas: (1) genetic studies to identify risk factors for common and rare diseases; (2) functional studies using human models, including iPSC-derived epithelial and immune models, to understand how genetic risk factors protect or predispose to disease; and (3) integrative human studies to identify biomarkers of important clinical outcomes, in particular response to molecularly-targeted therapies. Dr. Rioux’s work has led to over 200 publications, cited over 30,000 times."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "https://www.medgeni.org"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 1
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# Co-investigators

- Mark J Daly PhD (The Broad Institute; Massachusetts General Hospital)
- Ramnik Xavier MD (Massachusetts General Hospital; The Broad Institute)

# Lead Clinicians

- Alain Bitton MD (McGill University, McGill University Health Centre)
- Justin Côté-Daigneault (Université de Montréal, Centre Hospitalier de
  l’Université de Montréal)

# Primary Analyst

- Gabriel Boucher MSc

# Clinical Coordinators

- Kayleigh Morris, RN
- Julie Thompson Legault MSc

### NIDDK IBDGC-related funding

#### Montreal-Boston Collaborative IBD Genetic Research Center (DK062432; PI: Rioux)

The objective of this study is to identify the root causes of disease, so that we reveal the key targets for more precise and effective therapies for CD and UC in order to improve the quality of life of patients with CD and UC and eventually lead to the cure of these otherwise lifelong debilitating diseases.

**Specific Aim 1:**  Develop patient and control resources that will enhance the Consortium’s capacity to identify genes, genetic variation and biological mechanisms contributing to the pathogenesis of IBD.

**Specific Aim 2:** Pursue unique haplotype-based analyses in the French-Canadian population and integrate with Whole Exome Sequencing data to identify novel IBD causal variants.

**Specific Aim 3:** Perform targeted functional studies of putative variants to advance our knowledge of biological impact of protective and predisposing alleles on cellular functions.

#### Genotype-specific three-part model of human intestinal epithelium (DK062432; PI: Rioux)

The objective of this study is the development of robust and reproducible systems for generating and testing genotype-specific models of immune-epithelium-gut flora functions and interactions. Our strategy is based on human induced pluripotent stem cell (hiPSC) technology that had the potential of generating complex models combining intestinal epithelial cells, a variety of immune cells, as well as components of the intestinal flora, with the epithelial and immune cells being of specific genotypes.

#### Understanding how a loss of epigenetic reader SP140 contributes to IBD (DK119996)

NIDDK R01 Ancillary (K. Jeffrey, Leader; Rioux, PI of subaward)

The overall objective of the present proposal is to utilize multiple resources from the IBD Genetics Consortium to precisely define the roles of SP140 in mediating protective innate and adaptive immunity toward commensal microorganisms, and determine how altered expression of this epigenome reader in patients bearing common or rare variants of SP140 impacts homeostasis in the gut.

#### Major interests

- Genetic and genomic studies to identify risk factors for common and rare diseases;
- Functional studies using human models, including iPSC-derived epithelial and immune models, to understand how genetic risk factors protect or predispose to disease; and
- Integrative human studies to identify biomarkers of important clinical outcomes, in particular response to molecularly-targeted therapies.
