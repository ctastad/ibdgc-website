---
# Adapted from themes/academic/archetypes/project/index.md
title: "University of Toronto-Mount Sinai Hospital Genetic Research Center"
subtitle: ""
pi: "Mark Silverberg, MD, PhD, FRCPC"
pi_titles: ["Principal Investigator", "Professor of Medicine, University of Toronto", "Gastroenterologist, Department of Medicine", "Senior Investigator, Lunenfeld-Tanenbaum Research Institute Inflammatory Bowel Disease Group"]
picture: "/img/toronto/mark-silverberg.jpg"
#bio: ["After graduating from the University of Toronto, Faculty of Medicine in 1992, Dr. Mark Silverberg completed his internal medicine and gastroenterology training in Toronto in 1997. He then obtained a PhD studying the genetics of inflammatory bowel disease in 2002 at the Samuel Lunenfeld Research Institute of Mount Sinai Hospital. He is currently a Professor and Clinician Scientist in the Department of Medicine and holds the Gale and Graham Wright Research Chair in Digestive Diseases. He runs a laboratory based at Mount Sinai Hospital and the Lunenfeld-Tanenbaum Research Institute investigating the causes of inflammatory bowel disease (IBD).", "His research program has been funded by grants from the National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK/NIH), Canadian Institutes of Health Research (CIHR), Crohn's and Colitis Canada (CCC) and the Crohn's and Colitis Foundation of America (CCFA). His laboratory has focused on identifying susceptibility genes for IBD and to explain the contribution of genes and other biomarkers to its etiology and clinical course.", "More recently he has expanded his program to study the relationship between serum immune responses, gene regulation and the host microbiome with genetic susceptibility. He has made significant contributions to the discovery of genes related to Crohn's disease, ulcerative colitis and paediatric IBD. He also has made major contributions to clinical IBD research in the field of phenomics, classification of IBD (The Montreal Classification) and as well as in optimal use of biologic therapy and therapeutic drug monitoring.", "Dr. Silverberg has taken leadership positions on several international collaborative efforts with the goal of expediting scientific progress in the field of IBD. These include the International IBD Genetics Consortium, the NIDDK IBD Genetics Consortium and the CCFA Microbiome Initiative. Dr. Silverberg is also currently the Director of the Advanced IBD Fellowship Program at MSH and co-director of the Canadian GI Fellows Program in IBD.", "His current projects are directed toward understanding the relationship between the microbiome in the digestive tract and host genotype allowing more insight into the role of diet and how food may trigger or exacerbate IBD. Ultimately he hopes to develop tools that will allow clinicians to better predict who may develop IBD and to identify high-risk patients so that a more personalized approach to treatment based on patients' unique genetic and bacterial signatures may be employed. Dr. Silverberg also runs a large clinical practice focused on IBD at Mount Sinai Hospital."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "http://research.lunenfeld.ca/silverberg/"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 12
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# Co-investigators

- Ken Croitoru, MD, Professor of Medicine, University of Toronto, Gastroenterologist, Mount Sinai Hospital/Sinai Health System
- David Guttman, PhD, Professor, University of Toronto, Ecology & Evolutionary Biology, Director, University of Toronto Centre for the Analysis of Genome Evolution & Function (CAGEF)
- Mathieu Lupien, PhD, Senior Scientist, Princess Margaret Cancer Centre/OICR, Associate Professor, Dept. of Medical Biophysics, University of Toronto
- Daniel De Carvalho, PhD, Scientist, Princess Margaret Cancer Centre/OICR, Assistant Professor, Dept. of Medical Biophysics, University of Toronto

# Clinical Collaborators

- Hillary Steinhart MD
- Adam Weizman MD
- Geoffrey Nguyen MD, PhD
- Vivian Huang MD
- Zane Gallinger MD
- Laura Targownik MD

# Advanced IBD Fellows

- Cristian Hernandez MD
- Margaret Walshe MD
- Karen Boland, MD PhD
- Samuel Truniger MD

# Lab and Project Management

- Joanne Stempak MSc
- Melissa Filice MSc

---

### NIDDK IBDGC-related funding

#### Elucidating Pathophysiological Mechanisms of Intestinal Inflammation. Parent Grant 2017-2022

#### Specific Aims:

**Aim 1)** Identify gene expression profiles and pathways that will aid in understanding the mechanisms of UC relapse and prediction of UC relapse and their regulation by miRNAs and chromatin accessibility. This will be accomplished by measuring gene expression, miRNA and chromatic accessibility profiles in the colonic tissue of UC subjects immediately prior to the onset of inflammation relative to baseline measurements.

**Aim 2)** Identify intestinal microbes and microbial function that precede UC relapse. This will be accomplished by determining the composition of the microbial flora adherent to the intestinal tissue as well as that of stool.

**Aim 3)** Continue to support the mutually agreed upon Consortium-wide studies involving elucidating the factors contributing to relapse of inflammation following surgery in CD subjects, completing the discovery of all genetic variation associated with IBD, advancing our knowledge of the functional biology of such genetic variation and to utilize these data to bring clinically meaningful tools into use to promote improved outcomes for individuals affected by IBD. These will be accomplished by patient recruitment and by bringing our significant clinical and scientific expertise to the IBDGC.

#### Impact of an anti-inflammatory diet on disease activity and microbiome composition in active UC. Administrative Supplement

#### Specific Aims:

**Aim 1)** Elucidate the effects of diet on the microbiome community

**Aim 2)** Determine if inflammation and disease activity are affected by diet and its relationship to the microbial structure and function

#### Additional resources of value Mount Sinai Hospital/University of Toronto

Toronto local ileal CD recurrence cohort: The NIDDK Consortium-wide project focusing on genomic and microbial mechanisms of post-operative Crohn’s disease recurrence evolved out of a local Crohn’s and Colitis Canada grant received by the PI (Silverberg). An additional 45 subjects from the local study have been contributed to the overall Consortium project.

Prospective Pelvic Pouch Study cohort: A local Crohn’s and Colitis Canada funded study to understand the genomic and microbial factors that lead to new onset ileal inflammation in a pelvic pouch after colectomy in UC

GEM Cohort: Collaborator and co-investigator on the largest study world-wide to identify predictors and mechanisms of new onset CD in a genetically susceptible population of first-degree relatives of patients with CD (PI: Croitoru)

---

#### Major interests:

- Clinical and molecular classification of inflammatory bowel disease
- IBD therapeutics
- Multi-omic profiling of tissue from IBD patients to better understand genomic and microbial mechanisms of IBD onset, relapse and recurrence in human models of IBD (post-op CD, UC relapse, pouchitis)
