---
name: "Dr. Wei Chen"
role: "Associate Professor of Pediatrics"
picture: "/img/pittsburgh/wei_chen.png"
weight: 10
---

Dr. Wei Chen is Associate Professor of Pediatrics at the University of Pittsburgh School of Medicine, and the Director of Statistical Genetics Core with secondary appointments in the Department of Biostatistics and the Department of Human Genetics at the University of Pittsburgh Graduate School of Public Health. Dr. Chen received extensive training and experience in biostatistics, genetics, and genomics from Dr. Gonçalo Abecasis at the University of Michigan before joining the faculty at the University of Pittsburgh. His group is interested in method development and statistical analysis of omics data including DNA-seq, RNA-seq, methylation, ATAC-seq and single cell sequencing data. As a principal investigator, he has received NIH and foundation funded grants to develop statistical and computational methods for analyzing complex genomic data, and to develop methods for integrating multi-omics data. Recently, collaborating with his colleagues, his group started to use droplet-based single cell sequencing technology to study various diseases and published several computational methods for analyzing single cell data. Dr. Chen and his team are working closely with Drs. Duerr and Konnikova on the analysis and interpretation of CITE-seq, scRNA-seq, CyTOF and genotype data.
Email: wei.chen@chp.edu
Lab website: www.pitt.edu/~wec47

