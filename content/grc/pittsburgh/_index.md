---
# Adapted from themes/academic/archetypes/project/index.md
title: "IBD Genetics Research Center at the University of Pittsburgh"
subtitle: ""
pi: "Richard H. Duerr, MD"
pi_titles: ["Principal Investigator", "Inflammatory Bowel Disease Genetic Research Endowed Chair Professor of Medicine, Human Genetics, and Clinical and Translational Science, University of Pittsburgh", "Co-Director and Scientific Director, Inflammatory Bowel Disease Center UPMC"]
picture: "/img/pittsburgh/richard-duerr.jpg"
#bio: ["Richard H. Duerr, MD, is the Principal Investigator of the NIDDK IBDGC’s Genetic Research Center at the University of Pittsburgh, where he holds the endowed Inflammatory Bowel Disease Genetic Research Chair and is Professor of Medicine, Human Genetics, and Clinical and Translational Science.  He is the Co-Director and Scientific Director of the University of Pittsburgh Medical Center (UPMC) Inflammatory Bowel Disease Center, and he serves as Associate Chief Scientist, Translational Research on the Leadership Team of the Crohn’s & Colitis Foundation's IBD Plexus research and information exchange platform.  Dr. Duerr’s research career began with an IBD-focused research and clinical fellowship at UCLA, where he received training in cellular immunology and also participated in studies that established the association between the serum biomarker, p-ANCA, and IBD.  He refocused his research efforts on the genetics of IBD when he established an independent laboratory at the University of Pittsburgh.  Dr. Duerr currently applies his expertise in human genetics, genomics, and immunology to define the molecular effects of IBD-associated, non-coding, accessible chromatin region DNA variation on human CD4+CD45RO+CD196+ T cell functional responses to stimulation, and to conduct high dimensional immunophenotyping and transcriptional profiling of immunocytes isolated from peripheral blood and from inflamed and non-inflamed intestinal mucosa using the cellular indexing of transcriptomes and epitopes by sequencing (CITE-seq) assay."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "https://profiles.dom.pitt.edu/gi/faculty_info.aspx/Duerr5020"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 12
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# Co-investigators

- Wei Chen, PhD, Associate Professor of Pediatrics, Biostatistics, and Human
  Genetics, University of Pittsburgh

# Clinical Collaborators
- Arthur Barrie, MD, PhD
- David Binion, MD
- James Celebrezze, MD
- Jon Davison, MD
- Jeffrey Dueker, MD, MPH
- Janet Harrison, MD
- Jennifer Holder-Murray, MD
- Elyse Johnston, MD
- Kenneth Lee, MD
- David Medich, MD
- Marc Schwartz, MD
- Andrew Watson, MD

---

### NIDDK IBDGC-related funding

#### IBD Genetics Consortium Genetic Research Center (DK062420; PI: Duerr)

The objective is to continue our participation in NIDDK IBD Genetics Consortium studies utilizing a large cohort of IBD patients coupled with molecular genetics approaches to gain a better understanding of how human genetic variations predispose individuals to the dysregulation of the immune system that underlies IBD.

**Specific Aim 1:**  Participate in NIDDK IBDGC Steering Committee-approved studies.

**Specific Aim 2:**  Define the molecular effects of IBD-associated, non-coding, accessible chromatin region DNA variation on human CD4+CD45RO+CD196+ T cell functional responses to stimulation.

#### Administrative Supplements

#### IBD Genetics Consortium Genetic Research Center, Administrative Supplements (DK062420; PI: Duerr)

The goal is to conduct pilot projects on single cell multiomic analysis in colonoscopic biopsy-derived lamina propria mononuclear cells from patients with IBD.

#### Study of a Prospective Adult Research Cohort with IBD (SPARC IBD) (Crohn’ & Colitis Foundation; PI: Duerr)

The goals of this project are to collect and link clinical data, patient-reported outcome data, and serial biosamples through the course of the patients' disease, and to use the data and samples for basic, clinical, and translational research with the goal of finding predictors of response to therapy and predictors of relapse that will lead to precision medicine strategies and new therapeutic targets that will improve the quality of life of patients with IBD. Dr. Duerr facilitated the inclusion of SPARC IBD samples in the International IBD Genetics Consortium’s ongoing GWAS meta-analysis and whole exome sequencing projects.

---

#### Major interests

- Genetics of IBD;
- Molecular effects of IBD-associated, non-coding, accessible chromatin region DNA variation on human T cell functional responses to stimulation
- Single cell multiomic analysis in IBD.

