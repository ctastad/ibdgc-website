---
name: "Ta-Chiang Liu, MD, PhD"
role: "Assistant Professor of Pathology & Immunology, Washington University School of Medicine"
picture: "/img/cedars/ta-chiang_liu.png"
weight: 20
---

Dr. Liu is an Assistant Professor of Pathology and Immunology, Division of Anatomic and Molecular Pathology, Washington University School of Medicine. Dr. Liu’s main research interest is in the role of Paneth cells in Crohn’s Disease. In particular, he is interested in the molecular mechanisms of how morphologic patterns of cytoplasmic antimicrobial granules are affected by genetics and environmental triggers, and their clinical relevance. Dr. Liu’s research interests also include developing disease pathogenesis-relevant prognostic biomarkers.
