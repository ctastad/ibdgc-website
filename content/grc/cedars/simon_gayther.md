---
name: "Dr. Simon Gayther, PhD"
role: "Professor and Director of Molecular Epidemiology, Cedars-Sinai"
picture: "/img/cedars/simon_gayther.png"
weight: 30
---

Dr. Gayther’s research program is focused largely on understanding the underlying causes of ovarian cancer initiation and development. Dr. Gayther has a long, established track record in defining the heritable component of ovarian cancer, and the functional role of both common and rare risk variants and their target susceptibility genes in the early-stage disease pathogenesis. The overall approach of this research program is to integrate genomics and epigenomics analyses to identify molecular markers associated with disease, with cell biology modeling studies to validate the role of novel molecular markers in disease biology. The goal is to translate the findings from these studies into the clinical arena to improve risk prediction and prevention strategies, early-stage screening and disease diagnosis and targeted therapeutics for patients.
