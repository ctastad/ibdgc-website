---
# Adapted from themes/academic/archetypes/project/index.md
title: "Cedars-Sinai IBD Genetic Research Center"
subtitle: ""
pi: "Dermot McGovern, MD, PhD, FRCP(Lon)"
pi_titles: ["Principal Investigator", "Professor, Medicine and Biomedical Sciences", "Director, Translational Research in the Inflammatory Bowel and Immunobiology Research Institute"]
picture: "/img/cedars/dermot-mcgovern.jpeg"
#bio: ["Dermot McGovern completed his GI fellowship and obtained a PhD in complex disease genetics at the University of Oxford, England before moving to Cedars-Sinai, Los Angeles 11 years ago. Dr McGovern is the Director of Translational Medicine at the F. Widjaja Foundation Imflammatory Bowel and Immunobiology Research Institute at Cedars-Sinai and also directs the Cedars-Sinai Precision Medicine Initiative. He holds the Joshua and Lisa Greer Endowed Chair in IBD Genetics. Dr. McGovern’s clinical interests include the extra-intestinal complications of IBD and also the overlap between IBD and other immune-mediated and metabolic diseases. Dr. McGovern serves on the National Scientific Advisory Committee for the Crohn’s and Colitis Foundation as well as the steering committees of the National and International IBD Genetics Consortia. He also chairs the CCFA local advisory medical committee. Dr. McGovern serves on the editorial board for the IBD Journal. He has been elected a member of the American Society for Clinical Investigation (ASCI) and also the International Organization for the Study of IBD (IOIBD). Dr. McGovern’s group is funded by the NIH to study the effect of genetics on the susceptibility to and natural history of IBD in diverse populations. Dr McGovern’s group analyze multi’-omic’ datasets to identify potential biomarkers and therapeutic targets of IBD that ‘feed’ the Drug Development Unit he co-directs. His group also have a significant interest in the pharmacogenetics of therapies used in IBD. Extending these studies to diverse populations including African-Americans, Hispanics, and the Ashkenazi Jewish population is a major focus of his group. His group is also funded by the Crohn’s and Colitis Foundation, The Litwin Foundation, and the Helmsley Charitable Trust. Dr. McGovern’s group have published approximately 150 peer-reviewed papers in journals including Nature, Science, Cell, Nature Genetics, The Lancet, and Gastroenterology."]
tags: []
categories: []
date: 2020-08-07T09:46:16-04:00

# Optional external URL for PI
external_link: "https://www.cedars-sinai.org/research/labs/mccgovern.html"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.

publications:
    count: 12
    order: "desc"
---

<!-- manual color setting needed due to wowchemy page-wrapper breaking change -->
<body style="background-color: rgb(54, 70, 90);">

# Co-investigators

- Talin Haritunians PhD (Genomics)
- Thad Stappenbeck MD PhD (Epithelial Biology)
- Dalin Li PhD (Advanced Genomic Statistics)
- Ta-Chiang Liu MD PhD (Paneth Cells)
- Simon Gayther PhD (Functional Genomics)
- Dennis Hazelett PhD (Systems Biology)
- Arkadiusz Gertych PhD (Computational Pathology)
- Shishir Dube (Advanced Statistical Approaches to Imaging)
- Alka Potdar PhD (Transcriptomics)
- Jonathan Braun MD PhD (Mucosal/microbiome interaction)
- Janine Bilsborough PhD (Drug Discovery)
- Stephan Targan MD (Clinician)

# Additional Lead Clinicians

- Gil Melmed
- Phil Fleshner (surgeon)
- Shervin Rabizadeh and David Ziring (Pediatrics)
- Cindy Kallman (Radiology)
- Elena Chang (Pathology)
- Eric Vasiliauskas
- Tina Ha
- Gaurav Syal
- Nirupama Bonthala

# Clinical Coordinators

- Mary Hanna
- Justina Ibrahim
- Valeriya Pozdnyakova

# Lab Team Members
- Emebet Mengesha (Lab Manager)
- Philip Debbas
- Shell Shaohong (Phenotyper)

---

### NIDDK IBDGC-related goals

**Specific Aim 1:** Define genetic determinants of unmet medical needs in IBD. Sub-aim 1a: Define genetic factors associated with true non-response (TNR), pharmacokinetic, and immunogenic parameters of biologic therapies in IBD. Sub-aim 1b: Define genetic variation associated with perianal Crohn’s disease.

**Specific Aim 2:** Functional characterization of genetic risk variants associated with IBD. Sub-aim 2a: Characterization of the regulatory landscape in ileal CD tissues. Sub-aim 2b: Functional characterization of tissue specific regulatory and transcriptomic landscapes for IBD risk loci. Expression quantitative trait locus (eQTL) analysis of IBD risk regions. Connecting IBD-associated regulatory variants to target genes using chromosome conformation capture assays.

**Specific Aim 3:** Defining novel genetic determinants and environment interplay of Paneth cell phenotypes in CD patients. Determine the genetic associations, molecular mechanisms, and clinical relevance of Paneth cell defects in European ancestry, East Asian, African American, and Hispanic IBD patients.

#### Administrative Supplement

**Aim 1:** To quantify histomorphometric (HM) features within tissue architecture that are associated with disease recurrence following surgery for CD. Each category requires separate training data to develop a single classification algorithm that identifies all categories of interest at once in whole slide H&E images.

**Aim 2:** To determine the molecular underpinnings and genetic associations of HM features in CD and build prediction models for disease recurrence with integration of HM, genomic, clinical and transcriptomic features.

#### Ancillary liaisons

Jon Braun (Cedars): Biomarking IBD patient-specific disease features using the epithelial antigenic peptidome

---

#### Additional resources at Cedars-Sinai

- MIRIAD Biorepository: Contains over 15K IBD research subjects (includes extended families), for whom ~12,000 have whole exome or whole genome sequence data and GSA data. All subjects are consented for ‘call-backs’ for additional sampling as well as access to medical charts, radiology, histopathology etc.
- EBV Transformed Lymphoblastoid Cell Line Bank in ~15,000 subjects with WES and GSA in ~12,000. Capabilities to ‘drive’ these to IPSCs as well as ‘Gut-on-a Chip’ technology.
- Computational Pathology and Advanced Approaches to Imaging: Drs. Arkadiusz Gertych and Shishir Dube are bringing advanced computational approaches to histopathology and radiology, respectively, and leading efforts to integrate this with other ‘-omic’ data and clinical data across the GRCs.

#### Major interests

- Genetic and genomic studies to identify risk factors for IBD and also to study pleiotropic effects between IBD and other traits immune-mediated, metabolic, infectious, and neurological traits. (Additional collaborators: SARS-CoV2 (CORALE and CLARITY consortia); Parkinson’s Disease (Clive Svendsen))
- Genotype x Phenotype studies (Collaborators: VEOIBD & SHARE Consortia, Mike Kamm (POCER Study)).
- Genotype x Microbiome Studies (Additional Collaborators: Suzanne Devkota and David Underhill)
- Non-Northern European population studies: Ashkenazi Jews, East Asians, African Americans, Hispanics. (Additional Collaborators: Asan Medical Center, Korea; Tohoku University, Japan; Hailiang Hang; Children’s Hospital of LA; MiLAtinX Consortium, Marie Abreu; Esther Torres, University of Puerto Rico)
- Pharmacogenomics (Additional Collaborators: PANTS Consortium, Tariq Ahmad, Mark Daly)
- Sex-Specific Influences on IBD pathophysiology and Natural History
