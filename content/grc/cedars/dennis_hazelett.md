---
name: "Dr. Dennis Hazelett, PhD"
role: "Assistant Professor, Biomedical Sciences, Cedars-Sinai"
picture: "/img/cedars/dennis_hazelett.png"
weight: 40
---

Dr. Hazelett has a broad background in both experimental and computational biology. He relies on a systems-biology approach involving the analysis and synthesis of next-generation sequencing data. His most recent contributions are highlighted by efforts to annotate and draw inference from the functional consequences of genetic associations from genome-wide association studies (GWAS) for prostate cancer, Hodgkin’s lymphoma and ovarian cancers. Toward this goal, Dr. Hazelett integrated GWAS and next-generation sequencing data from my collaborators with public datasets such as1000 genomes and the Encyclopedia of DNA elements (ENCODE) to infer new diseases variants outside of transcribed sequences. He has recently begun independent efforts as a principal investigator at Cedars-Sinai Medical Center, using novel approaches to achieve a greater understanding of the relationship between the regulatory code and genetic disease variants.
